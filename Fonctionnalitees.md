Fonctionnalitees
------

42lforms proposera:
- plusieurs types de champs pour vos formulaires.    
  champ text (grandeur variables), radio, checkbox, curseurs, dates ...
- Systeme de template pour les formulaires
- Feedback a la fin du formulaire
- format de reponse obligatoire
- Creation de statistique automatique pour la reponses
- Formulaire public, privee (mot de passe, lien unique, ...)
- CSS customisable