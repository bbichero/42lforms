Presentation
------

42lforms sera un outils de creation de formulaires open source et    
soucieux de la vie privee de ces utilisateurs.    
A la maniere du fameux google forms, notre outil sera, utilisable    
et deployable par n'importe qui.    
Le premier but est de fournir une application simple pour creer,    
partager et analyser des formulaires.    

Notre outil sera completement deployable par docker, chaque service    
interne sera dockeiser pour utiliser le moins de ressource possible.    